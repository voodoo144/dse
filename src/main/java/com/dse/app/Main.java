package com.dse.app;

import com.dse.config.Config;
import com.dse.database.Database;
import com.dse.services.impl.IndexerService;
import com.dse.services.impl.WatcherService;
import com.dse.services.impl.WebService;
import org.apache.commons.cli.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Created by Алексей on 22.09.2014.
 */
public class Main {
	final static Logger logger = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) {
		logger.info("Starting application");

		Option web = new Option("web", "run web application api");
		Option help = new Option("help", "print this message");
		Option configFile = OptionBuilder
				                    .withArgName("config")
				                    .hasArg()
				                    .withDescription("use given config file")
				                    .create("config");
		Option indexer = new Option("indexer", "run application in indexer mode");
		Option version = new Option("version", "print the version information and exits");
		Option watcher = new Option("watcher","run watchers");

		Options options = new Options();
		options.addOption(web);
		options.addOption(help);
		options.addOption(configFile);
		options.addOption(indexer);
		options.addOption(version);
		options.addOption(watcher);

		CommandLineParser cli = new BasicParser();
		logger.info("Creating cli parser");
		try {
			logger.info("Parsing command line arguments");
			CommandLine cmd = cli.parse(options, args);
			logger.info("Found this options: " + cmd.toString());
			if (cmd.hasOption("help") || cmd.getOptions().length == 0) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("dse", options);
				System.exit(0);
			}

			if (cmd.hasOption("config")) {
				Config.setFilePath(cmd.getOptionValue("config"));
			} else {
				logger.warn("Starting without config file: all settings will have default values");
			}

			if (cmd.hasOption("indexer")) {
				logger.info("Starting indexer in dedicated thread");
				Thread index=new Thread(new IndexerService());
				index.setName("indexer");
				index.start();
			}

			if (cmd.hasOption("web")) {
				logger.info("Starting web server in dedicated thread");
				Thread server=new Thread(new WebService());
				server.setName("server");
				server.start();
			}

			if(cmd.hasOption("watcher")){
				logger.info("Starting watcher service in dedicated thread");
				Thread watcherService=new Thread(new WatcherService());
				watcherService.setName("watcher-service");
				watcherService.start();
			}

			if (cmd.hasOption("version")) {
				logger.info("Version: 1.0 SNAPSHOT");
			}

		} catch (ParseException e) {
			logger.error(e.getMessage());
		}

	}
}