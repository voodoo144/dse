package com.dse.config;

import org.ini4j.Ini;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by Алексей on 22.09.2014.
 */
public class Config {
	Logger logger = LoggerFactory.getLogger(Config.class);
	private static volatile Config instance;
	private static String filePath;

	public static void setFilePath(String path){
		filePath=path;
	}

	public static Config getInstance() {
		Config localInstance = instance;
		if (localInstance == null) {
			synchronized (Config.class) {
				localInstance = instance;
				if (localInstance == null) {
					instance = localInstance = new Config(filePath);
				}
			}
		}
		return localInstance;
	}

	private Ini ini = null;
	private Ini.Section web = null;
	private Ini.Section indexer = null;
	private Ini.Section watcher = null;
	private Ini.Section database = null;

	private Config(String filePath) {
		logger.info("Creating config from file:" + filePath);
		try {
			this.ini = new Ini(new File(filePath));
			logger.info("Config file content:" + ini.toString());
		} catch (Exception e) {
			logger.error(e.toString());
		}
		this.web = ini.get("web");
		this.indexer = ini.get("indexer");
		this.watcher = ini.get("watcher");
		this.database = ini.get("database");
	}

	public Ini.Section getWeb() {
		return web;
	}

	public void setWeb(Ini.Section web) {
		this.web = web;
	}

	public Ini.Section getIndexer() {
		return indexer;
	}

	public void setIndexer(Ini.Section indexer) {
		this.indexer = indexer;
	}

	public Ini.Section getWatcher() {
		return watcher;
	}

	public void setWatcher(Ini.Section watcher) {
		this.watcher = watcher;
	}

	public Ini.Section getDatabase() {
		return database;
	}

	public void setDatabase(Ini.Section database) {
		this.database = database;
	}


}
