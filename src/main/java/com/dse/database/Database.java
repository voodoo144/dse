package com.dse.database;

import com.dse.config.Config;
import com.dse.models.impl.Document;
import com.dse.models.impl.SearchQuery;
import com.dse.models.impl.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Алексей on 25.09.2014.
 */
public class Database {
	private Logger logger = LoggerFactory.getLogger(Database.class);
	private final Config config=Config.getInstance();
	private static volatile Database instance;

	public static Database getInstance() {
		Database localInstance = instance;
		if (localInstance == null) {
			synchronized (Database.class) {
				localInstance = instance;
				if (localInstance == null) {
					instance = localInstance = new Database();
				}
			}
		}
		return localInstance;
	}

	private Database(){
		try {
			Class.forName(config.getDatabase().get("db.class"));
			logger.info("Connecting to database...");
			this.connection = DriverManager.getConnection(config.getDatabase().get("db.path"),
			                                              config.getDatabase().get("db.user"),
			                                              config.getDatabase().get("db.pass")
			);
		} catch (Exception e) {
			logger.error(e.toString());
		}
	}

	private Connection connection;

	private void createDatabase() throws SQLException {
		Statement s = this.connection.createStatement();
		String createDatabaseQuery = "CREATE DATABASE " + config.getDatabase().get("db.name") + ";";
		logger.info("Execute create db: " + createDatabaseQuery);
		s.executeUpdate(createDatabaseQuery);
	}

	private void createTables() throws SQLException {
		Statement s = this.connection.createStatement();
		String createUsersTableQuery = "CREATE TABLE users(" +
				                               "  id serial PRIMARY KEY NOT NULL," +
				                               "  username text," +
				                               "  password text," +
				                               "  is_admin int DEFAULT 0," +
				                               "  creation_date timestamp," +
				                               "  creator text" +
				                               ");";
		logger.info("Execute create users table: " + createUsersTableQuery);
		s.executeUpdate(createUsersTableQuery);
		User defaultAdmin = new User() {{
			setUsername("admin");
			setPassword("admin");
			setAdmin(true);
		}};
		createUser(defaultAdmin);
		String createDocumentsTableQuery = "";
		logger.info("Execute create documents table:" + createDocumentsTableQuery);
	}

	public void createUser(User user) throws SQLException {
		Statement s = this.connection.createStatement();
		s.executeUpdate("INSERT INTO users (" +
				                "username," +
				                "password," +
				                "is_admin," +
				                "creation_date," +
				                "creator" +
				                ") VALUES (" +
				                user.getUsername() + "," +
				                user.getPassword() + "," +
				                user.getAdmin() + "," +
				                "CURRENT_TIMESTAMP()," +
				                "SYSTEM" +
				                ");");
	}

	public void createDocument(Document document) {}

	public void updateDocument(String id, Document newDocument) {}

	public void deleteDocument(String id) {}

	public List<Document> findDocumentsByQuery(SearchQuery query) throws SQLException {
		return new ArrayList<Document>();
	}

	public List<Document> findAllDocuments() {
		return new ArrayList<Document>();
	}

	public Document findDocumentById(String id) {
		return new Document();
	}

	public User findUserByObject(User user) throws SQLException {
		Statement s = connection.createStatement();
		ResultSet rs = s.executeQuery("SELECT * from users where username='" + user.getUsername() + "' and " +
				                              "password='" +
				                              user.getPassword() + "';");
		rs.next();
		String username = rs.getString("username");
		String password = rs.getString("password");
		boolean isAdmin = (rs.getInt("is_admin") == 1);
		return new User(username, password, isAdmin);
	}

	public void deleteUserById(String id) throws SQLException {

	}
}
