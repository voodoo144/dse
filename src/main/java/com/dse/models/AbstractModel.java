package com.dse.models;

import com.google.gson.Gson;

import java.lang.reflect.Type;

/**
 * Created by Алексей on 23.10.2014.
 */
public abstract class AbstractModel {

	public static <T> T fromJson(String json,Type type){
		return new Gson().fromJson(json,type);
	}

	public String toJson(){
		return new Gson().toJson(this);
	}

}
