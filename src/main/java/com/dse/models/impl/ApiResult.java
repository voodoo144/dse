package com.dse.models.impl;

import com.dse.models.AbstractModel;

/**
 * Created by Алексей on 02.10.2014.
 */
public class ApiResult extends AbstractModel {
	public static final String SUCCESS_STATUS="SUCCESS";
	public static final String ERROR_STATUS="ERROR";

	private String error;
	private String status;
	private Object result;
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

}
