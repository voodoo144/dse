package com.dse.models.impl;

import com.dse.models.AbstractModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Алексей on 29.09.2014.
 */
public class Document extends AbstractModel {

	private String location;
	private Date creationDate;
	private String name;
	private Date modificationDate;
	private Date deletionDate;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Date getDeletionDate() {
		return deletionDate;
	}

	public void setDeletionDate(Date deletionDate) {
		this.deletionDate = deletionDate;
	}
}
