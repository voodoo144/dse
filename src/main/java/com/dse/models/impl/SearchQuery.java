package com.dse.models.impl;

import com.dse.models.AbstractModel;

/**
 * Created by Алексей on 29.09.2014.
 */
public class SearchQuery extends AbstractModel {
	private String query;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
}
