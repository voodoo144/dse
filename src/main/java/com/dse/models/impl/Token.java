package com.dse.models.impl;

import com.dse.models.AbstractModel;

/**
 * Created by Алексей on 30.09.2014.
 */
public class Token extends AbstractModel {
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
