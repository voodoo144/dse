package com.dse.models.impl;

import com.dse.models.AbstractModel;

/**
 * Created by Алексей on 29.09.2014.
 */
public class User extends AbstractModel {

	public User(){}
	public User(String username,String password,boolean isAdmin){
		this.username=username;
		this.password=password;
		this.admin=isAdmin;
	}

	private String username;
	private String password;
	private boolean admin = false;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean getAdmin() {
		return this.admin;
	}
}
