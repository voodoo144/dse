package com.dse.security;

import com.auth0.jwt.*;
import com.auth0.jwt.impl.BasicPayloadHandler;
import com.auth0.jwt.impl.JwtProxyImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Алексей on 29.09.2014.
 */
public class TokenGenerator {

	private Logger logger = LoggerFactory.getLogger(TokenGenerator.class);

	private String secret;
	private Algorithm algorithm = Algorithm.HS256;
	private JwtProxy proxy = new JwtProxyImpl();
	private ClaimSet claimSet = new ClaimSet();


	public TokenGenerator(String secret) {
		proxy.setPayloadHandler(new BasicPayloadHandler());
		claimSet.setExp(24 * 60 * 60);//exp in 24 hours
		this.secret = secret;
	}

	public String encode(Object obj) {
		String token = null;
		try {
			token = proxy.encode(algorithm, obj, secret, claimSet);
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return token;
	}

	public Object decode(String token) throws Exception{
		return proxy.decode(algorithm, token, secret);
	}
}
