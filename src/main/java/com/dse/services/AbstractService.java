package com.dse.services;

import com.dse.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Алексей on 25.09.2014.
 */
public abstract class AbstractService {
	private final Config config=Config.getInstance();
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	public Config getConfig() {
		return config;
	}

}
