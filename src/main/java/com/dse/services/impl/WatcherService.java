package com.dse.services.impl;

import com.dse.config.Config;
import com.dse.services.AbstractService;
import com.dse.watchers.FolderWatcher;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Алексей on 25.09.2014.
 */
public class WatcherService extends AbstractService implements Runnable {

	@Override
	public void run() {
		String dirList=getConfig().getWatcher().get("watcher.dir");
		int i=0;
		for(String path:dirList.split(";")){
			i++;
			logger.info("Will watch dir: "+path);
			Path folder= Paths.get(path.trim());
			Thread watcher=new Thread(new FolderWatcher(folder));
			watcher.setName("folder-watcher-"+i);
			watcher.start();
		}
	}
}
