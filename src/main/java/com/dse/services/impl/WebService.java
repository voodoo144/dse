package com.dse.services.impl;

import com.dse.database.Database;
import com.dse.models.impl.*;
import com.dse.security.TokenGenerator;
import com.dse.services.AbstractService;
import com.dse.transformers.JsonTransformer;

import java.util.*;

import static spark.Spark.*;
import static spark.SparkBase.setPort;
import static spark.SparkBase.staticFileLocation;

/**
 * Created by Алексей on 25.09.2014.
 */
public class WebService extends AbstractService implements Runnable {
	private String apiVersion;
	private TokenGenerator tokenGenerator;
	private Database database = Database.getInstance();

	public WebService() {
		apiVersion = getConfig().getWeb().get("web.api.version");
		final int port = Integer.parseInt(getConfig().getWeb().get("web.port"));
		final String publicDir = getConfig().getWeb().get("web.static.dir");
		final String secret = getConfig().getWeb().get("web.secret");

		tokenGenerator = new TokenGenerator(secret);

		logger.info("Api version:" + apiVersion);
		logger.info("Port:" + port);
		setPort(port);
		if (publicDir != null) {
			logger.warn("Overwriting static files location, location set to:" + publicDir);
			externalStaticFileLocation(publicDir);
		} else {
			logger.info("Using standart static file location:/public");
			staticFileLocation("/public");
		}

	}

	public void run() {
		before("/api/*", (request, response) -> {
			final Token clientToken = Token.fromJson(request.body(), Token.class);
			final String token = clientToken.getToken();
			logger.info("Got token from client: " + token);
			logger.info("Checking permissions");
			try {
				Object internalObject = tokenGenerator.decode(token);
				User user = User.fromJson(internalObject.toString(), User.class);
				if (user.getAdmin()) {
					logger.info("User has admin rights");
				} else {
					logger.info("Basic user");
				}
			} catch (Exception e) {
				logger.error("Invalid token");
				halt("Wrong token");
			}
		});

		get("/", (request, response) -> {
			logger.info("GET /");
			response.redirect("/index.html");
			logger.info("Redirected to /index.html");
			return "";
		});

		post("/auth", "application/json", (request, response) -> {
			logger.info("POST /auth");
			final ApiResult result = new ApiResult();
			String body = request.body();
			logger.info("With body:" + body);
			User user = User.fromJson(body, User.class);
			try {
				User databaseUser = database.findUserByObject(user);
				String token = tokenGenerator.encode(user);
				result.setStatus(ApiResult.SUCCESS_STATUS);
				result.setResult(databaseUser);
				result.setToken(token);
			} catch (Exception e) {
				result.setStatus(ApiResult.ERROR_STATUS);
				result.setError(e.getMessage());
			}
			return result;
		}, new JsonTransformer());

		get("/api" + apiVersion + "/documents", (request, response) -> {
			final ApiResult result = new ApiResult();
			logger.info("GET /api/v1/documents");
			result.setResult(database.findAllDocuments());
			return result;
		}, new JsonTransformer());

		get("/api/" + apiVersion + "/document/:id", (request, response) -> {
			final ApiResult result = new ApiResult();
			logger.info("GET /api/v1/document/:id");
			String documentId = request.params(":id");
			database.findDocumentById(documentId);
			return result;
		}, new JsonTransformer());

		put("/api/" + apiVersion + "/document/:id/update", "application/json", (request, response) -> {
			final ApiResult result = new ApiResult();
			logger.info("PUT /api/v1/document/:id/update");
			String documentId = request.params(":id");
			database.updateDocument(documentId, new Document());
			result.setResult("UPDATED");
			result.setStatus(ApiResult.SUCCESS_STATUS);
			return result;
		}, new JsonTransformer());

		delete("/api/" + apiVersion + "/document/:id/delete", (request, response) -> {
			final ApiResult result = new ApiResult();
			logger.info("DELETE /api/v1/document/:id/delete");
			String documentId = request.params(":id");
			database.deleteDocument(documentId);
			result.setResult("DELETED");
			result.setStatus(ApiResult.SUCCESS_STATUS);
			return result;
		}, new JsonTransformer());

		post("/api/" + apiVersion + "/document/create", "application/json", (request, response) -> {
			ApiResult result = new ApiResult();
			logger.info("POST /api/v1/document/create");
			database.createDocument(new Document());
			result.setResult("documentId");
			result.setStatus(ApiResult.SUCCESS_STATUS);
			return result;
		}, new JsonTransformer());

		post("/api/" + apiVersion + "/search", "application/json", (request, response) -> {
			ApiResult result = new ApiResult();
			logger.info("POST /api/v1/search");
			String body = request.body();
			SearchQuery searchQuery = SearchQuery.fromJson(body, SearchQuery.class);
			try {
				List<Document> docs = database.findDocumentsByQuery(searchQuery);
				result.setResult(docs);
				result.setStatus(ApiResult.SUCCESS_STATUS);
			} catch (Exception e) {
				result.setStatus(ApiResult.ERROR_STATUS);
				result.setError(e.getMessage());
			}
			return result;
		}, new JsonTransformer());

		post("/api/" + apiVersion + "/user/create", "application/json", (request, response) -> {
			final ApiResult result = new ApiResult();
			logger.info("POST /api/v1/user/create");
			String body = request.body();
			User user = User.fromJson(body, User.class);
			try {
				database.createUser(user);
				result.setResult(user);
				result.setStatus(ApiResult.SUCCESS_STATUS);
			} catch (Exception e) {
				result.setError(e.getMessage());
				result.setStatus(ApiResult.ERROR_STATUS);
			}
			return result;
		}, new JsonTransformer());

		delete("/api/" + apiVersion + "user/:id/delete", "application/json", (request, response) -> {
			final ApiResult result = new ApiResult();
			logger.info("DELETE /api/v1/user/:id/delete");
			String id = request.params(":id");
			try {
				database.deleteUserById(id);
				result.setResult("DELETED");
				result.setStatus(ApiResult.SUCCESS_STATUS);
			} catch (Exception e) {
				result.setStatus(ApiResult.ERROR_STATUS);
				result.setError(e.getMessage());
			}
			return result;
		}, new JsonTransformer());
	}
}
