package com.dse.transformers;

import com.google.gson.Gson;
import spark.ResponseTransformer;

/**
 * Created by Алексей on 23.10.2014.
 */
public class JsonTransformer implements ResponseTransformer {
	@Override
	public String render(Object o) throws Exception {
		return new Gson().toJson(o);
	}
}
