package com.dse.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Алексей on 23.09.2014.
 */
public class Base64 {
	private final static Logger logger = LoggerFactory.getLogger(Base64.class);

	public static String decode(String src) {
		logger.info("Trying to decode string:" + src);
		String decodedString = "";
		if (src != null) {
			decodedString = new String(java.util.Base64.getDecoder().decode(src));
		}
		logger.info("Decoded result:" + decodedString);
		return decodedString;
	}

	public static String encode(String src) {
		logger.info("Trying to encode string;" + src);
		String encodedString = "";
		if (src != null) {
			encodedString = java.util.Base64.getEncoder().encodeToString(src.getBytes());
		}
		logger.info("Encoded result:" + encodedString);
		return encodedString;
	}
}
