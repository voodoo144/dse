package com.dse.watchers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Created by Алексей on 23.10.2014.
 */
public class FolderWatcher implements Runnable {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private Path path;

	public FolderWatcher(Path path) {
		logger.info("Creating folder watcher for "+path);
		this.path = path;
	}

	@Override
	public void run() {
		FileSystem fs = this.path.getFileSystem();
		try (WatchService ws = fs.newWatchService()) {
			this.path.register(ws, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);
			WatchKey key = null;
			logger.info("Folder watcher created");
			while (true) {
				key = ws.take();
				Path dir=(Path)key.watchable();
				Kind<?> kind = null;
				for (WatchEvent<?> watchEvent : key.pollEvents()) {
					kind = watchEvent.kind();
					if (OVERFLOW == kind) {
						continue;
					}
					if (ENTRY_CREATE == kind) {
						final Path newPath = getEventPath(dir,watchEvent);
						logger.info("New path created: " + newPath);
					}
					if (ENTRY_DELETE == kind) {
						final Path newPath = getEventPath(dir,watchEvent);
						logger.info("Path was deleted: "+ newPath);
					}
					if (ENTRY_MODIFY == kind) {
						final Path newPath = getEventPath(dir,watchEvent);
						logger.info("Path was modifying: "+newPath);
					}
				}
				if (!key.reset()) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	public void getFileAttrs(Path path) throws IOException{
		BasicFileAttributes attrs=Files.readAttributes(path, BasicFileAttributes.class);
		logger.info(attrs.creationTime().toString());
	}

	private Path getEventPath(Path parentDir,WatchEvent watchEvent){
		return parentDir.resolve(((WatchEvent<Path>)watchEvent).context());
	}
}
